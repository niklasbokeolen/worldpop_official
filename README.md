# worldpop_official

script files for worldpop manuscript
[REF]


Functions to create world population projections that follows both RCP and SSP scenarios
Author: Niklas Boke Olén
Contact: niklasbokeolen@gmail.com


### Running

The file CMIP6_worlpop_2010_unique.R needs to be run first. 
It requires input of:
* RCP Landuse, urban fraction for one scenario (for reference of pixel size) (https://luh.umd.edu/  LUH2_v2f_beta)
* World map shape for country extent (http://thematicmapping.org/downloads/)
* distance to urban center raster (created with euclidian distance tool on data https://www.sciencedirect.com/science/article/pii/S0303243420308989)
* distance to road raster (created with euclidian distacne tool on data https://sedac.ciesin.columbia.edu/data/set/groads-global-roads-open-access-v1)

Once the above step is run the file

CMIP6_population_gridding_world_aurora.R can be run
It is setup to run on multiple cores (number is specified in file)

This requires an additional input of
* SSP population projections (https://tntcat.iiasa.ac.at/SspDb/dsd?Action=htmlpage&page=welcome)
* RCP landuse projections (https://luh.umd.edu/  LUH2_v2f_beta)


Other
* Some of the main scripts are located in world_pop_functions3.R which is loaded by the above script.
